defmodule OpenIdAuth.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Finch, name: OpenIdAuth.Finch.Pool},
      OpenIdAuth.Cache
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: OpenIdAuth.Application.Supervisor)
  end
end
