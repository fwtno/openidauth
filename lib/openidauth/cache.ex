defmodule OpenIdAuth.Cache do
  @moduledoc """
  Simple cache with support for generating values
  """
  use Agent

  def start_link(opts \\ []) do
    {initial, opts} = Keyword.pop(opts, :initial, %{})
    opts = Keyword.put_new(opts, :name, __MODULE__)
    Agent.start_link(fn -> initial end, opts)
  end

  @doc """
  Get or generate a key

  ## Example

  ```
  > OpenIdAuth.Cache.get("test", ttl: 3600, )
  ```
  """
  def get(key, opts \\ []) do
    pid_or_name = opts[:pid] || __MODULE__
    generator = opts[:generator]

    now = DateTime.utc_now()
    {exists?, res} = Agent.get(pid_or_name, fn m -> {Map.has_key?(m, key), Map.get(m, key)} end)

    cond do
      # value does exist and has not expired
      exists? && (nil == res[:ttl] || :lt == DateTime.compare(now, res[:ttl])) ->
       {:ok, res[:value]}

      # no value and no way to generate one
      not exists? && not is_function(generator, 0) ->
        {:error, {:not_found, key}}

      is_function(generator, 0) ->
        # if a generator exists and the value has expired generate a
        # new value
        with {:ok, value} <- generator.() do
          update(key, value, opts)
        end
    end
  end

  @doc """
  Get a cached key
  """
  def update(key, value, opts \\ []) do
    pid_or_name = opts[:pid] || __MODULE__

    ttl =
      case opts[:ttl] do
        nil ->
          nil

        ttl ->
          DateTime.add(DateTime.utc_now(), ttl)
      end

    :ok = Agent.update(pid_or_name, &Map.put(&1, key, %{ttl: ttl, value: value}))

    {:ok, value}
  end
end
