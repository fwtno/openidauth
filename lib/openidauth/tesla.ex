# set per module
defmodule OpenIdAuth.Tesla do
  use Tesla

  adapter(Tesla.Adapter.Finch, name: OpenIdAuth.Finch.Pool)
end
