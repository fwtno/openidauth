defmodule OpenIdAuth.OAuth2 do
  @moduledoc """
  Wrapper around common OAuth2 functionality
  """
  require Logger

  def client(opts \\ []) do
    # we are not dependent on CentientWeb, endpoint must be given as argument
    clientdef = Application.get_env(:openidauth, __MODULE__)

    {site, _ignore} = Keyword.pop(clientdef, :site)

    {:ok, newopts} = defaults_from_well_known(site)

    {endpoint, opts} = get_redirect_endpoint(opts)

    newopts
    |> Keyword.merge(opts)
    |> Keyword.merge(clientdef)
    |> Keyword.put_new(:redirect_uri, "#{endpoint}/auth/callback")
    |> OAuth2.Client.new()
  end

  @doc """
  Get the URL for which to redirect for authorization
  """
  def authorize_url!(client \\ client()) do
    client
    |> OAuth2.Client.put_param(:state, nonce(16))
    |> OAuth2.Client.put_param(:scope, "email profile")
    |> OAuth2.Client.authorize_url!()
  end

  @doc """
  Get the url redirect to for a logout
  """
  def logout_url!(client \\ client()) do
    {:ok, opts} = defaults_from_well_known(client.site)
    opts[:end_session_endpoint]
  end

  @doc """
  Make a OAuth request with a specific client
  """
  def get(client, url, headers \\ [], opts \\ []) do
    OAuth2.Client.get(client, url, headers, opts)
  end

  @doc """
  Get a token from a code
  """
  def get_token!(client \\ client(), code) do
    OAuth2.Client.get_token!(client, code: code)
  end

  def get_userinfo(client \\ client()) do
    {:ok, opts} = defaults_from_well_known(client.site)
    get(client, opts[:userinfo_endpoint] || "#{client.site}/userinfo")
  end

  @doc """
  Get the APIs defined in /.well-known/openid-configuration
  """
  def defaults_from_well_known(nil), do: {:ok, []}
  def defaults_from_well_known(site) do
    OpenIdAuth.Cache.get(site, ttl: 3600, generator: fn ->
      resp =
      OAuth2.Client.new([])
      # serializer may not be present
      |> OAuth2.Client.put_serializer("application/json", Jason)
      |> OAuth2.Client.get("#{site}/.well-known/openid-configuration")

      case resp do
        {:ok, %{status_code: 200, body: body}} ->
          IO.inspect body
          opts = [
            site: site,
            issuer: body["issuer"],
            strategy: pick_strategy(body["grant_types_supported"]),
            # the naming as used in OAuth2.Client
            authorize_url: body["authorization_endpoint"],
            token_url: body["token_endpoint"],
            # The keys available, token and authorized are same values as
            # above but here with correct naming as per oauth
            token_endpoint: body["token_endpoint"],
            authorization_endpoint: body["authorization_endpoint"],
            end_session_endpoint: body["end_session_endpoint"],
            userinfo_endpoint: body["userinfo_endpoint"],
            revocation_endpoint: body["revocation_endpoint"],
            jwks_uri: body["jwks_uri"]
          ]

          {:ok, opts}

        {:error, err}  ->
          Logger.warning("Failed to retrieve openid-configuration: #{inspect err}")
          {:error, err}
      end
    end)
  end

  defp nonce(maxlen) do
    make_ref()
    |> :erlang.term_to_binary()
    |> then(&:crypto.hash(:sha256, &1))
    |> Base.encode32(case: :lower, padding: false)
    |> String.slice(0, maxlen)
  end

  defp pick_strategy(nil), do: OAuth2.Strategy.AuthCode
  defp pick_strategy([]), do: nil
  defp pick_strategy(["authorization_code" | _]), do: OAuth2.Strategy.AuthCode
  defp pick_strategy(["client_credentials" | _]), do: OAuth2.Strategy.ClientCredentials
  defp pick_strategy([_ | rest]), do: pick_strategy(rest)

  defp get_redirect_endpoint(opts) do
    defendpoint =
      case Application.get_env(:openidauth, :endpoint) do
        "" <> _ = endpoint ->
          endpoint

        mod when is_atom(mod) ->
          mod.url()
      end

    Keyword.pop(opts, :endpoint, defendpoint)
  end
end
