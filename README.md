# OpenID Auth - Elixir

Simplify the setup of an openid connect endpoint using the `.well-known` standard
location to autoconfigure the client.

##  Installation

```elixir

  defp deps() do
    [
      {:openidauth, "~> 0.1"}
    ]
  end
end
```


## Usage

`:openidauth` must be configured for your oauth provider to work. Only a single
provider is supported at the moment.

```elixir
# config/config.exs
config :openidauth, :endpoint, MyPhoenixWebApp.Endpoint

# Using ORY - replace site, client_id and client_secret
config :openidauth, OpenIdAuth.OAuth2,
  site: "https://smart-atomic-qsdasdasd6.projects.oryapis.com",
  client_id: "654f084d-f4c1-4a4b-88b0-698d37cbb88e"
  client_secret: "gVkasdj4j56_ 61imasdnasdn1",
  serializers: %{
    "application/json" => Jason
  },
  strategy: OAuth2.Strategy.AuthCode

# Using Auth0 - replace site, client_id and client_secret
config :openidauth, OpenIdAuth.OAuth2,
  site: "https://dev-qjgmfnanfdi5asda.eu.auth0.com",
  client_id: "KMksoa94mdmad9DFADm134afakasdmga",
  client_secret: "41asd95196_asdADS49139mfFmghi5n1ASDmgh0asd8924n139ASdmfa913n5619",
  serializers: %{
    "application/json" => Jason
  },
  strategy: OAuth2.Strategy.AuthCode


# Using Google GSuite - replace client_id and client_secret
config :openidauth, OpenIdAuth.OAuth2,
  site: "https://accounts.google.com/",
  client_id: "9581056819058-4onWUUISC6Gz5Y6qn6Tt8z3CAC40jwgv.apps.googleusercontent.com",
  client_secret: "GOCSPX-zv-kASAD31mAsfhA954m1ASfax",
  serializers: %{
    "application/json" => Jason
  },
  strategy: OAuth2.Strategy.AuthCode
```

Normal OAuth2 authentication with plugs, guardian or whatever method may then be used.
