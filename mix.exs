defmodule OpenIdAuth.MixProject do
  use Mix.Project

  def project do
    [
      app: :openidauth,
      version: "0.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      elixirc_options: [warnings_as_errors: true],
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  def application do
    [
      mod: {OpenIdAuth.Application, []},
      extra_applications: []
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp aliases(), do: []

  defp deps do
    [
      {:oauth2, "~> 2.0"},
      {:tesla, "~> 1.5"},
      {:finch, "~> 0.13"},
    ]
  end
end
